package com.myntra.sof;

import java.io.InputStream;
import java.util.logging.LogManager;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.bridge.BridgeOptions;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.eventbus.bridge.tcp.TcpEventBusBridge;

public abstract class BaseModule  extends AbstractVerticle {
	
	public Logger logger;
	
	private String moduleName;
	private String moduleId;
	private static final  String DEFAULT_MODULE_NAME = "BASE_MODULE";
	private static final  String DEFAULT_MODULE_ID = "BASE_MODULE_ID";

	@Override
	  public void start(Future<Void> startFuture) throws Exception {
		super.start(startFuture);

		JsonObject config = config();

		initLogger();

		if (config != null) {
			logger.info("Passed Config Params " + config.toString());
			setupModule();
		} else {
			logger.info("No config parameters passed while starting Vertx");
		}

		//logger.info("Port NO " + Integer.valueOf(config.getJsonObject("ebConfig").getJsonObject("bridgeInfo").getString("bridgePortNo")));

		if (config.getJsonObject("ebConfig").getJsonObject("bridgeInfo").getString("enabled").equalsIgnoreCase("true")) {
			TcpEventBusBridge bridge = TcpEventBusBridge.create(vertx,
					new BridgeOptions()
							.addInboundPermitted(new PermittedOptions().setAddressRegex(
									config.getJsonObject("ebConfig").getJsonObject("bridgeInfo").getString("inBoundAddress")))
							.addOutboundPermitted(new PermittedOptions().setAddressRegex(
									config.getJsonObject("ebConfig").getJsonObject("bridgeInfo").getString("outBoundAddress")))
			);

			try {
				/*Fix Me*/
				bridge.listen(Integer.valueOf(config.getJsonObject("ebConfig").getJsonObject("bridgeInfo").getString("bridgePortNo")),
                        result -> {
					if (result.failed()) {
						logger.warn("Not able to start the TCP Event Bus bridge on port 7000");
						throw new RuntimeException(result.cause());
					} else {
						logger.info("TCP Event Bus bridge running on port 7000");
					}
				});
			} catch (Exception e) {
				logger.warn("Caught Exception - while listening to com.myntra.sof");
			}
		} else {
			logger.info("Event Bus Bridge Not Required to be setup");
		}
	}

	protected abstract void setupModule();
	
	protected abstract void startModule(Message message);

	protected abstract void stopModule(Message message);

	protected abstract void getModuleDetails(Message message);

	protected abstract void setModuleDetails(Message message);

    protected abstract void healthCheck(Message message);
	
	@Override
	public void stop(Future<Void> stopFuture) throws Exception {
		super.stop(stopFuture);
	}
	
	private void initLogger(){
		try{
	        final InputStream inputStream = BaseModule.class.getResourceAsStream("/logging.properties");
	        LogManager.getLogManager().readConfiguration(inputStream);
	        logger = (Logger) LoggerFactory.getLogger(BaseModule.class.getName());
	        logger.info("Basemodule Logger Initialized");
		}catch(Exception ex){
	    	logger.info("Not able to initialize logger");
	    }
    }
}
