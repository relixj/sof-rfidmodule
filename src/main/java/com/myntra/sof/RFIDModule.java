package com.myntra.sof;

import com.myntra.sof.rfidmodule.modulecommunicator.ModuleCommunicator;
import com.myntra.sof.rfidmodule.rfiddevicecommunicator.RFIDDeviceCommunicator;
import com.myntra.sof.utils.EventListener;
import com.myntra.sof.utils.IEventListener;
import com.myntra.sof.utils.Response;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.ClientAuth;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.net.JksOptions;

public  class RFIDModule extends BaseModule implements IEventListener {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RFIDModule.class.getName());

    private static final String COMMAND = "COMMAND";
	private Integer moduleStatus = Constants.AgentConstants.STOPPED;

	public String moduleAddress = null;
    public String agentAddress = null;
	private String moduleName;
	private String moduleId;

	private RFIDDeviceCommunicator deviceCommunicator ;
	private ModuleCommunicator moduleCommunicator;

	@Override
	protected void setupModule() {

		/*Called only once */

		setModuleName(config().getJsonObject("moduleInfo").getString("moduleName"));
		setModuleID(config().getJsonObject("moduleInfo").getString("moduleID"));

        VertxOptions options = new VertxOptions()
                .setEventBusOptions(new EventBusOptions()
                        .setSsl(false)
                        .setKeyStoreOptions(new JksOptions().setPath("keystore.jks").setPassword("wibble"))
                        .setTrustStoreOptions(new JksOptions().setPath("keystore.jks").setPassword("wibble"))
                        .setClientAuth(ClientAuth.REQUIRED)
                );

        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {

                moduleAddress = config().getJsonObject("moduleInfo").getString("moduleAddress");
                agentAddress = config().getJsonObject("ebAgentInfo").getString("agentAddress");

                logger.info("My Module Address is "+ moduleAddress);
                logger.info("My Agent Address is "+ agentAddress);

				EventListener.getInstance().registerAddress(moduleAddress);

                /* Add listeners to Events */
                EventListener.getInstance().addListener(moduleAddress, this);

                /*Posting Ready message to Agent */
                Response response = new Response();

                response.command = Constants.AgentCommands.START;
                response.moduleAddress = moduleAddress;
                response.message = new JsonObject().put("state",Constants.AgentConstants.READY);
                response.status = Constants.SUCCESS;

                EventListener.getInstance().postMessage(agentAddress,response.jsonResponse(response));

                //dummyStartModule();

            } else {
                logger.info("Failed: " + res.cause());
            }
        });
	}

	private void dummyStartModule(){

        if(null == deviceCommunicator && null == moduleCommunicator){

            JsonObject config = config();

            deviceCommunicator = new RFIDDeviceCommunicator(config);
            moduleCommunicator = new ModuleCommunicator(config);

			deviceCommunicator.setupCommunication();
            deviceCommunicator.startDevice();

            moduleCommunicator.setupModuleCommunication();

            logger.info("Module setup done\n");
        }
        else{
            logger.info("Module Already Running\n");
        }
    }

	@Override
	protected void startModule(Message message) {

		if(moduleStatus == Constants.AgentConstants.STOPPED &&
                (null == deviceCommunicator  && null == moduleCommunicator)){

		        logger.info("Module Start Command Received");

                JsonObject config = config();

                deviceCommunicator = new RFIDDeviceCommunicator(config);
                moduleCommunicator = new ModuleCommunicator(config);

                deviceCommunicator.setupCommunication();
                deviceCommunicator.startDevice();

                moduleCommunicator.setupModuleCommunication();

                logger.info("startModule - Module has started");
                logger.info("startModule - publishing response to Agent @ " + message.replyAddress());

                Response response = new Response();

                response.command = Constants.AgentCommands.START;
                response.moduleAddress = moduleAddress;
                response.message = new JsonObject().put("state",Constants.AgentConstants.START_PENDING);
                response.status = Constants.SUCCESS;

                message.reply(response.jsonResponse(response));
//                JsonObject body = new JsonObject();
//                body.put("Module","Started");
//                DeliveryOptions dos = new DeliveryOptions();;
//                dos.setSendTimeout(1000);
//                dos.addHeader("Status","Healthy");
//                message.reply(body,dos);
                logger.info("Response sent to "+ message.replyAddress());

		}else{
            Response response = new Response();

            response.command = Constants.AgentCommands.START;
            response.moduleAddress = moduleAddress;
            response.message = new JsonObject().put("state",Constants.AgentConstants.STARTED);
            response.status = Constants.FAILURE;

            logger.info("startModule - Module is already running - start called again");
            logger.info("startModule - publishing response to Agent @ " + message.replyAddress());
            message.reply(response.jsonResponse(response));
            logger.info("Response sent to "+ message.replyAddress());
		}

	}

	@Override
	protected void stopModule(Message message) {

		if(moduleStatus == Constants.AgentConstants.STARTED){
			deviceCommunicator.stopDevice();
			deviceCommunicator = null;

            Response response = new Response();
            response.moduleAddress = moduleAddress;
            response.command = Constants.AgentCommands.STOP;
            response.message = new JsonObject().put("state",Constants.AgentConstants.STOPPED);
            response.status = Constants.SUCCESS;

			logger.info("stopModule - Module Stopped");
            logger.info("stopModule - publishing response to Agent @ " + message.replyAddress());
            logger.info("Response is "+ response.toString());
            message.reply(response.jsonResponse(response));
            logger.info("Response sent to "+ response.toString());
		}else{

            Response response = new Response();
            response.moduleAddress = moduleAddress;
            response.command = Constants.AgentCommands.STOP;
            response.message = new JsonObject().put("state",Constants.AgentConstants.STOPPED);
            response.status = Constants.FAILURE;

            logger.info("stopModule - Module has stopped already - stop called again");
            logger.info("stopModule - publishing response to Agent @ " + message.replyAddress());
            message.reply(response.jsonResponse(response));
            logger.info("Response sent to "+ response.toString());
		}
	}

    private String getModuleID(){
	    return moduleId;
    }

	private void setModuleID(String modID){
	    moduleId = modID;
    }

    private String getModuleName()
    {
        return moduleName;
    }

	private void setModuleName(String modName)
    {
        moduleName = modName;
    }

    @Override
    protected void healthCheck(Message message){

	    EventListener.getInstance().postMessage(message.replyAddress(),deviceCommunicator.getDeviceHealth());
    }

    public void getModuleDetails(Message message){
        Response response = new Response();
        response.moduleAddress = moduleAddress;
        response.command = Constants.AgentCommands.GET_MODULE_DETAILS;
        response.message = new JsonObject().put("moduleName",getModuleName()).put("moduleId",getModuleID());
        response.status = Constants.SUCCESS;

        EventListener.getInstance().postMessage(message.replyAddress(),response.jsonResponse(response));
    }

    public void setModuleDetails(Message message){

        JsonObject json = (JsonObject) message.body();
        setModuleName(json.getString("ModuleName"));
        setModuleID(json.getString("ModuleID"));

        Response response = new Response();
        response.moduleAddress = moduleAddress;
        response.command = Constants.AgentCommands.SET_MODULE_DETAILS;
        response.message = new JsonObject().put("state","SET");
        response.status = Constants.SUCCESS;

        EventListener.getInstance().postMessage(message.replyAddress(),response.jsonResponse(response));

    }

    /*To be refactored*/
	public void onEvent(Message message) {

		String address = message.address();
		JsonObject json = (JsonObject) message.body();

        Integer command = json.getInteger(COMMAND);

		logger.info("Message For "+address +"Command is "+command);

		switch(command){

            case Constants.AgentCommands.START:
                startModule(message);
                break;

            case Constants.AgentCommands.STOP:
				stopModule(message);
                break;

            case Constants.AgentCommands.HEALTH_CHECK:
                healthCheck(message);
                break;

            case Constants.AgentCommands.GET_MODULE_DETAILS:
                getModuleDetails(message);
                break;

            case Constants.AgentCommands.SET_MODULE_DETAILS:
                setModuleDetails(message);
                break;

            default:
                logger.info("onEvent - No Handler Registered ");
                break;
        }
		logger.info("onEvent - Handling Done");
	}
}