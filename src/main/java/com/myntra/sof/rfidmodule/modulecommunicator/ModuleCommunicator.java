package com.myntra.sof.rfidmodule.modulecommunicator;

import com.myntra.sof.rfidmodule.rfiddevicecommunicator.ITagReceiver;
import com.myntra.sof.rfidmodule.rfiddevicecommunicator.TagListener;
import com.myntra.sof.utils.EventListener;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.mqtt.MqttClient;
import io.vertx.ext.web.client.HttpRequest;


public class ModuleCommunicator implements IModuleCommunicator, ITagReceiver {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(ModuleCommunicator.class.getName());
    private static WebClient webClient = WebClient.create(Vertx.currentContext().owner());

    private Vertx vertx = Vertx.currentContext().owner();
    JsonObject config = null;

    private MqttClient mqttClient = MqttClient.create(vertx);

    public ModuleCommunicator(JsonObject config){
        this.config = config;
    }


    public void setupModuleCommunication() {
        logger.info("Setting up pdp communication");
        TagListener.getInstance().addListener(this);
    }

    public void onTags(JsonObject json) {

        logger.info("onTags called");

        if (json != null) {
            logger.info("RFID_MAC_UUID -  " + json.getValue("RFID_MAC_UUID"));
            logger.info("TAG_READ_COUNT -  " + json.getValue("TAG_READ_COUNT"));
            logger.info("RFID_TAG_LIST -  " + json.getValue("RFID_TAG_LIST"));
        }

        sentTagsHTTP(json);
        //sendTagsLocal(json);
        //sendTagsMqtt(json);
    }

    private void sendTagsMqtt(JsonObject json){

//        String tagTopic = config.getJsonObject("mqttServerInfo").getString("tagTopic");
//        Integer port = Integer.valueOf(config.getJsonObject("mqttServerInfo").getString("mqttPort"));
//        String mqttHost = config.getJsonObject("mqttServerInfo").getString("mqttHost");

        logger.info("Sending tags over mqtt");


        mqttClient.connect(1883, "10.50.66.83", s -> {
            logger.info("Connected to Mqtt Client");

            json.put("StoreId","Roadster-Mantri");

            mqttClient.publish("sof.myntra.pdp.rfidmodule",
                    Buffer.buffer(json.toString()),
                    MqttQoS.AT_LEAST_ONCE,
                    false,
                    false);
            logger.info("Sending Mqtt Event to Topic - sof.myntra.pdp.rfidmodule");

        });
    }

    private void sendTagsLocal(JsonObject json){

        String pdpAddr = config.getJsonObject("ebModuleEndpoints").getString("pdpBaseAddr");

        logger.info("Posting a message to "+pdpAddr);

        EventListener.getInstance().postMessage(pdpAddr,json);
    }

    private void sentTagsHTTP(JsonObject body){

        String url = config.getJsonObject("httpServerInfo").getString("serverURL") +
                config.getJsonObject("httpServerInfo").getString("tagPostEndPoint");

        logger.info("resolveTags - RFID_MAC_UUID -  " + body.getValue("RFID_MAC_UUID"));
        logger.info("resolveTags - TAG_READ_COUNT -  " + body.getValue("TAG_READ_COUNT"));
        logger.info("resolveTags - RFID_TAG_LIST -  " + body.getValue("RFID_TAG_LIST"));

        logger.info("resolveTags - Requesting - URL "+ url);

        body.put("store_id",1);

        webClient
                .postAbs(url)
                .putHeader("Content-Type", "application/json; charset=utf-8")
                .sendJsonObject(body, ar -> {
            if (ar.succeeded()) {
                // Obtain response
                HttpResponse<Buffer> response = ar.result();
                //JsonObject json = response.bodyAsJsonObject();

                logger.info("resolveTags - Received response with status code " + response.statusCode());
                logger.info("resolveTags - Received response body " + response.body());

            } else {
                logger.info("resolveTags - Something went wrong " + ar.cause().getMessage());
            }
        });
    }
}
