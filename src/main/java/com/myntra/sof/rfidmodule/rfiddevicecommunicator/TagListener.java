package com.myntra.sof.rfidmodule.rfiddevicecommunicator;

import java.util.ArrayList;

import io.vertx.core.json.JsonObject;

public final class TagListener{

	private static TagListener tagListener;

	private TagListener(){
	}

	public static TagListener getInstance(){
		if(null ==  tagListener){
			tagListener = new TagListener();
		}
		return tagListener;
	}

	ArrayList tagListeners = new ArrayList<ITagReceiver>();
	
	public void addListener(ITagReceiver tagReceiver){
		tagListeners.add(tagReceiver);
	}

	public void notifyListeners(JsonObject json){
		
		System.out.println("rfidListeners.size()"+tagListeners.size());

		for(int i=0; i< tagListeners.size(); i++){
			ITagReceiver tagReceiver = (ITagReceiver) tagListeners.get(i);
			//logger.info("Calling tagReceiver.receiveTags");
			tagReceiver.onTags(json);
		}
	}

}