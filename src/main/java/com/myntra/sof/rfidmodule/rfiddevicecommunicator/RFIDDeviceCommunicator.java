package com.myntra.sof.rfidmodule.rfiddevicecommunicator;
import com.myntra.sof.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.myntra.sof.utils.EventListener;
import com.myntra.sof.utils.Response;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;


public class RFIDDeviceCommunicator implements IRfidDeviceCommunicator {

	public String agentAddress = null;
    public String moduleAddress = null;

    private static final Logger logger = (Logger) LoggerFactory.getLogger(RFIDDeviceCommunicator.class.getName());
    private Vertx vertx = Vertx.currentContext().owner();
	JsonObject config = null;

	private HttpServer server;
	public RoutingContext routingContext;

    public RFIDDeviceCommunicator(JsonObject config){
        this.config = config;
    }

	@Override
	public void startDevice() {
		vertx.<String>executeBlocking(future -> {
			try {

			    //String startCommand = config.getJsonObject("deviceInfo").getString("startCommand");

			    //logger.info("startDevice: "+startCommand);
				agentAddress = config.getJsonObject("ebAgentInfo").getString("agentAddress");
                moduleAddress = config.getJsonObject("moduleInfo").getString("moduleAddress");

                Runtime rt = Runtime.getRuntime();
                String[] commands = {"whoami"}; /* Open Technowave EXE here - Figure Out Later */
                Process proc = null;
                proc = rt.exec(commands);

                BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

                BufferedReader stdError = new BufferedReader(new
                        InputStreamReader(proc.getErrorStream()));

                // read the output from the command
                logger.info("Here is the standard output of the command:\n");

                String s = null;
                while ((s = stdInput.readLine()) != null) {
                    logger.info(s);
                }

                // read any errors from the attempted command
                logger.info("Here is the standard error of the command (if any):\n");
                while ((s = stdError.readLine()) != null) {
                    logger.info(s);
                }
                } catch (IOException e) {
                    e.printStackTrace();
                }

				logger.info("Sent STARTED Message to Agent " + agentAddress);

                future.complete();
		}, res -> {

			if (res.succeeded()) {
                Response response = new Response();

                response.command = Constants.AgentCommands.START;
                response.moduleAddress = moduleAddress;
                response.message = new JsonObject().put("state",Constants.AgentConstants.STARTED);
                response.status = Constants.SUCCESS;

                EventListener.getInstance().postMessage(agentAddress,response.jsonResponse(response));

				logger.info("All Good\n");
			} else {
				res.cause().printStackTrace();
			}
		});
	}

	@Override
	public void stopDevice() {
		
	}

	@Override
	public void getDeviceConfig() {
		
	}

	@Override
	public void setDeviceConfig() {
		
	}

	@Override
	public JsonObject getDeviceDetails() {
		return null;
	}

	@Override
	public void deactivateTags() {
		
	}

	@Override
	public void encodeTags() {
		
	}

	@Override
	public JsonObject getDeviceHealth() {

		JsonObject json = new JsonObject();
		json.put("Status","Running");

		return json;
	}

	@Override
	public void setupCommunication() {
		startServer();
    }

	private void startServer() {

		Integer port = Integer.valueOf(config.getJsonObject("deviceInfo").getJsonObject("deviceAPI").getString("port"));

        logger.info("Port "+port.toString());

		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());

		router.post("/storeassist/rfid/reportitems").handler(this::handleRFIDMessages);

		server = vertx.createHttpServer().requestHandler(router::accept).listen(port);

		if(server != null)
		{
			logger.info("Server Created Successfully\n");
		}
	}

	private void handleRFIDMessages(RoutingContext routingContext) {

		JsonObject json = routingContext.getBody().toJsonObject();

		logger.info(json);

		TagListener.getInstance().notifyListeners(json);

		routingContext.request().response().end("Success");
	}
}