package com.myntra.sof.rfidmodule.rfiddevicecommunicator;

import io.vertx.core.json.JsonObject;

public interface ITagReceiver{

	void onTags(JsonObject json);
}
