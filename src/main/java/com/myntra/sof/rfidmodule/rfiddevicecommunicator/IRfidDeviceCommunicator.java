package com.myntra.sof.rfidmodule.rfiddevicecommunicator;

import io.vertx.core.json.JsonObject;

public interface IRfidDeviceCommunicator{

    public void startDevice();

    public void stopDevice();

    public void getDeviceConfig();

    public void setDeviceConfig();

    public JsonObject getDeviceHealth();

    public JsonObject getDeviceDetails();

    public void deactivateTags();

    public void encodeTags();

    public void setupCommunication();
}



