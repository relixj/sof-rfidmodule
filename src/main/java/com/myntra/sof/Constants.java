package com.myntra.sof;

public class Constants {
    public static final int FAILURE = 0;
    public static final int SUCCESS = 1;

    public static class AgentCommands {
        public static final int START =  0xA001;
        public static final int STOP =  0xA002;
        public static final int HEALTH_CHECK =  0xA003;
        public static final int GET_CONFIG =  0xA004;
        public static final int SET_CONFIG =  0xA005;
        public static final int RESTART =  0xA006;
        public static final int GET_MODULE_DETAILS =  0xA007;
        public static final int SET_MODULE_DETAILS =  0xA008;
    }

    //Return/Status values supported by Agent
    public static class AgentConstants{

        public static final int READY =  0xA101;
        public static final int STARTED = 0xA102;
        public static final int START_PENDING = 0xA103;
        public static final int STOPPED = 0xA104;
        public static final int STOP_PENDING = 0xA105;
        public static final int RUNNING = 0xA106;
        public static final int SUCCESS = 0xA107;
        public static final int FAILED = 0xA108;
    }

    //Commands supported by RFID Module which can be called by other Modules
    public static class RFIDModuleCommands {

        public static final int ENCODE_TAGS = 0x1001;
        public static final int KILL_TAGS = 0x1002;
        public static final int DEACTIVATE_TAGS = 0x1003;
        public static final int CHECK_TAG_STATUS = 0x1004;
        public static final int TAG_LOCATION = 0x1005;
        public static final int RESTART_COMMAND = 0x1006;
    }
}