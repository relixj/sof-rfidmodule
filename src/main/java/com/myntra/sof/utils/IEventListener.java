package com.myntra.sof.utils;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

public interface IEventListener{

	void onEvent(Message message);

}