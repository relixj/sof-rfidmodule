package com.myntra.sof.utils;

import java.util.HashMap;
import java.util.Map.Entry;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.Iterator;

import io.vertx.core.eventbus.Message;

public final class EventListener{

    private static final Logger logger = (Logger) LoggerFactory.getLogger(EventListener.class.getName());
    private static Vertx vertx = Vertx.currentContext().owner();

    private static EventListener eventListener;
    private static EventBus eb;

    private EventListener(){

    }

	public static EventListener getInstance(){
		if(null ==  eventListener){
            eb = vertx.eventBus();
			eventListener = new EventListener();
		}
		return eventListener;
	}

    //Fix Me - Only one callback per address is implemented. Take a listener list as param
    HashMap<String, IEventListener> eventListeners = new HashMap<String,IEventListener>();
    
	public void addListener(String address, IEventListener listener){
        eventListeners.put(address, listener);
        logger.info("Adding Listener "+ listener + " to Addr - " + address);
    }
    
    public void removeListener(String address){
        eventListeners.remove(address);
        logger.info("Removed eventListener "+ address +" New - Size "+ eventListeners.size());
	}

	public void notifyListeners(Message message){
        String address = message.address();

        logger.info("notifyListeners "+ address);
        
        Iterator itr = (Iterator) eventListeners.entrySet().iterator();
        IEventListener listener;
         
        while(itr.hasNext())
        {
            HashMap.Entry<String, IEventListener> entry = (Entry<String, IEventListener>) itr.next();
            //logger.info("Input Key " + address + "Hash Key = " + entry.getKey() + ", Value = " + entry.getValue());

            if(0 == address.compareTo(entry.getKey())){
                logger.info("Found a listener");
                listener = (IEventListener) entry.getValue();
                listener.onEvent(message);
                logger.info("Notified "+ address);
            }
        }
    }

    public void onMessage(Message message) {
        /* All Incoming Messages on Event Bus should come through this*/

        logger.info("onMessage - Address "+message.address());

        try {

            eventListener = EventListener.getInstance();

            if(null != eventListener){
                eventListener.notifyListeners(message);
                logger.info("On Message Done!");
            }
            else{
                logger.info("Message from + "+ message.address() + "couldn't reach as expected");
            }
        }
        catch (Exception e) {
            logger.warn("Not able to post message from "+message.address());
            throw new RuntimeException(e);
        }
    }


    public void registerAddress(String address){

        eb.consumer(address, message -> {
            onMessage(message);
        });
    }

    public void postMessage(String address, JsonObject data) {
        /* All OutGoing Messages on Event Bus should go through this*/
        //Implement Me

        if(eb!= null){
            logger.info("Address - " + address);

            eb.publisher(address).send(data, res -> {
                if (res.succeeded()) {
                    logger.info("Successfully sent message to "+address);
                }
                else{
                    logger.info("Post Message Failed");
                    logger.info(res.cause(),res);
                }
            });
        }
    }
}