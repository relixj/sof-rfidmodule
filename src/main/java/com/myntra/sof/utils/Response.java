
package com.myntra.sof.utils;

import io.vertx.core.json.JsonObject;

public class Response {

	public int 			command;
	public String 		moduleAddress;
	public JsonObject 	message;
	public int 			status;

	public Response() {
		super();
	}

	public JsonObject jsonResponse(Response obj)
	{
		JsonObject json = new JsonObject();

		json.put("command",obj.command);
        json.put("moduleAddress",obj.moduleAddress);
		json.put("response",obj.message);
		json.put("status",obj.status);

		return json;
	}

}