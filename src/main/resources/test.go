package main

import "fmt"
import "github.com/hashicorp/memberlist"
import "sync"
import "time"

func blockForever() {
    wg := sync.WaitGroup{}
    wg.Add(1)
    wg.Wait()
}
// this is a comment

func main() {
    fmt.Println("Hello World")
    list, err := memberlist.Create(memberlist.DefaultLocalConfig())
if err != nil {
	panic("Failed to create memberlist: " + err.Error())
}

// Join an existing cluster by specifying at least one known member.
n, err := list.Join([]string{"192.168.13.32"})
if err != nil {
	panic("Failed to join cluster: " + err.Error())
}


for i := 0; i < 100; i++ {

// Ask for members of the cluster
for _, member := range list.Members() {
	fmt.Printf("Member: %s %s\n", member.Name, member.Addr)
}

time.Sleep(5 * time.Second)

}
_ = n
blockForever()
}
